package com.nexsoft.formative.models.service;

import javax.transaction.TransactionScoped;

import com.nexsoft.formative.models.entities.City;
import com.nexsoft.formative.models.repository.CityRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@TransactionScoped
public class CityService {
    @Autowired
    CityRepository repo;

    public void saveCity(City city){
        repo.save(city);
    }
    
}
