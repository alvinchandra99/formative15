package com.nexsoft.formative.models.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "province")
public class Province implements Serializable{

    @Id
    private int id;

    private String name;

    private String code;

    private Date deleteCode;

    private int countryId;

    @OneToMany
    private List<City> city;

    

    public Province(int id, String name, String code, Date deleteCode, int countryId, List<City> city) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.deleteCode = deleteCode;
        this.countryId = countryId;
        this.city = city;
    }

    public Province() {
    }



    public List<City> getCity() {
        return city;
    }

    public void setCity(List<City> city) {
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getDeleteCode() {
        return deleteCode;
    }

    public void setDeleteCode(Date deleteCode) {
        this.deleteCode = deleteCode;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    

    

}