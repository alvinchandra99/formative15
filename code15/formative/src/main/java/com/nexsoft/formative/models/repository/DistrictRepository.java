package com.nexsoft.formative.models.repository;

import com.nexsoft.formative.models.entities.District;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DistrictRepository extends CrudRepository<District, Integer> {

    District save(District country);

    
}
