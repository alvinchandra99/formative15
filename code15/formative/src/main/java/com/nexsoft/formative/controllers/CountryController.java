package com.nexsoft.formative.controllers;

import com.nexsoft.formative.models.service.CityService;
import com.nexsoft.formative.models.service.CountryService;
import com.nexsoft.formative.models.service.DistrictService;
import com.nexsoft.formative.models.service.ProvinceService;
import com.nexsoft.formative.models.entities.City;
import com.nexsoft.formative.models.entities.Country;
import com.nexsoft.formative.models.entities.District;
import com.nexsoft.formative.models.entities.Province;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CountryController {

    @Autowired
    CountryService countryService;

    @Autowired
    ProvinceService provinceService;

    @Autowired
    CityService cityService;

    @Autowired
    DistrictService districtService;

    @GetMapping("/")
    public String welcome(){
        return "Welcome";
    }

    @PostMapping("/")
    public void insertCountry(@RequestBody Country country){
        System.out.println(country.getProvince());

        countryService.saveCountry(country);
        
        for(Province province : country.getProvince()){
            provinceService.saveProvince(province);
            for(City city: province.getCity()){
                cityService.saveCity(city);
                for(District district : city.getDistrict()){
                    districtService.saveDistrict(district);
                }
            }
        }
    }

    @GetMapping("/country/{id}")
    public Country showCountryById(@PathVariable int id){
        return countryService.findById(id);
    }

    
    
}
