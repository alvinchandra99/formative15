package com.nexsoft.formative.models.repository;

import com.nexsoft.formative.models.entities.City;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CityRepository extends CrudRepository<City, Integer> {

    City save(City country);

    
}
