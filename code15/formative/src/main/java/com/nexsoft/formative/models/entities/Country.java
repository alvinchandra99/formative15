package com.nexsoft.formative.models.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "country")
public class Country implements Serializable {
    

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private Date deleteDate;

    private String code;

    @OneToMany
    private List<Province> province;



    public Country(int id, String name, Date deleteDate, String code, List<Province> province) {
        this.id = id;
        this.name = name;
        this.deleteDate = deleteDate;
        this.code = code;
        this.province = province;
    }


    public List<Province> getProvince() {
        return province;
    }


    public void setProvince(List<Province> province) {
        this.province = province;
    }

    public Country() {
    }
    
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(Date deleteDate) {
        this.deleteDate = deleteDate;
    }

    
    
}
