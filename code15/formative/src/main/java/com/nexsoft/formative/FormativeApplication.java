package com.nexsoft.formative;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FormativeApplication {

	public static void main(String[] args) {
		SpringApplication.run(FormativeApplication.class, args);
	}

}
