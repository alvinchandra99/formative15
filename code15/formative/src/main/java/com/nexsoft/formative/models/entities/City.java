package com.nexsoft.formative.models.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "city")
public class City implements Serializable {
    private String code;

    private String name;

    private Date deleteDate;

    @Id
    private int id;

    private int countryId;

    private int provinceId;

    @OneToMany
    private List<District> district;

    public City() {
    }

    public City(String code, String name, Date deleteDate, int id, int countryId, int provinceId,
            List<District> district) {
        this.code = code;
        this.name = name;
        this.deleteDate = deleteDate;
        this.id = id;
        this.countryId = countryId;
        this.provinceId = provinceId;
        this.district = district;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(Date deleteDate) {
        this.deleteDate = deleteDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public List<District> getDistrict() {
        return district;
    }

    public void setDistrict(List<District> district) {
        this.district = district;
    }

    

    

    
}
