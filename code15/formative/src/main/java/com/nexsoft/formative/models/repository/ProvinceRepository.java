package com.nexsoft.formative.models.repository;

import com.nexsoft.formative.models.entities.Province;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProvinceRepository extends CrudRepository<Province, Integer> {

    Province save(Province country);

    
}
