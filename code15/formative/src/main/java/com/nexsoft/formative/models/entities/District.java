package com.nexsoft.formative.models.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "District")
public class District implements Serializable {

    @Id
    private int id;

    private String code;

    private String name;

    private Date deleteDate;

    private int countryId;

    private int provinceId;

    private int cityId;

    public District() {
    }

    public District(int id, String code, String name, Date deleteDate, int countryId, int provinceId, int cityId) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.deleteDate = deleteDate;
        this.countryId = countryId;
        this.provinceId = provinceId;
        this.cityId = cityId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(Date deleteDate) {
        this.deleteDate = deleteDate;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    

    

    
}
