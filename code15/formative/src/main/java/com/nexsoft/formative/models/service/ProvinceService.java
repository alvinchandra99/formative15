package com.nexsoft.formative.models.service;

import javax.transaction.TransactionScoped;

import com.nexsoft.formative.models.entities.Province;
import com.nexsoft.formative.models.repository.ProvinceRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@TransactionScoped
public class ProvinceService {
    
    @Autowired
    ProvinceRepository repo;

    public void saveProvince(Province province){
        repo.save(province);
    }
    
}
