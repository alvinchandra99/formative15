package com.nexsoft.formative.models.service;

import javax.transaction.Transactional;

import com.nexsoft.formative.models.entities.Country;
import com.nexsoft.formative.models.repository.CountryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class CountryService {

    @Autowired
    private CountryRepository repo;

    public Country findById(int id){
        return repo.findById(id);
    }

    public void saveCountry(Country country){
        repo.save(country);
    }

    
    
}
