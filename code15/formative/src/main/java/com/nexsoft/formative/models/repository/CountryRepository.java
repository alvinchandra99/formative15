package com.nexsoft.formative.models.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.nexsoft.formative.models.entities.Country;
import java.util.List;

@Repository
public interface CountryRepository extends CrudRepository <Country, Integer>{
    Country findById(int id);
    List<Country> findAll();
    void deleteById(int id);
    Country save(Country country);
 
    
}
