package com.nexsoft.formative.models.service;

import javax.transaction.TransactionScoped;

import com.nexsoft.formative.models.entities.District;
import com.nexsoft.formative.models.repository.DistrictRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@TransactionScoped
public class DistrictService {

    @Autowired
    DistrictRepository repo;

    public void saveDistrict(District district){
        repo.save(district);
    }

    
}
